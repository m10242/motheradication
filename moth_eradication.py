class Location: 
    def __init__(self, x, y): 
        self.x = x 
        self.y = y 

def inputs(no_of_traps) :
    list_of_points = []
    x_list = []
    y_list = []
    for i in range(no_of_traps):
        x, y = [float(i) for i in input().split()]
        x_list.append(x)
        y_list.append(y)
        list_of_points.append(Location(x, y))
    vertices_of_polygon = polygon_vertices(list_of_points,len(list_of_points))
    return vertices_of_polygon, x_list, y_list

import math

def finding_perimeter(list_of_points) ->str:
    perimeter = 0
    for i in range(0, len(list_of_points)):
        p1 = list_of_points[i]
        if i == len(list_of_points) - 1 :
            break
        else :
            p2 = list_of_points[i+1]
        perimeter += math.sqrt(((p2[0] - p1[0]) ** 2) + ((p2[1] - p1[1]) ** 2))
    return round(perimeter, 2)

def polygen_vertices(list_of_points, no_of_traps):
    if no_of_traps < 3: 
        return False
    bottomleft_point = finding_initial_Point(list_of_points) 
    hull = [] 
    fp = bottomleft_point
    sp = 0
    while True: 
        hull.append(fp) 
        sp = (fp + 1) % no_of_traps
        for i in range(no_of_traps): 
            if orientation(list_of_points[fp], list_of_points[i], list_of_points[sp]) == 2: 
                sp = i 
        fp = sp 
        if(fp == bottomleft_point): 
            break
    vertices = []
    begin_vertex = []
    for trap in hull:
        vertices.append([list_of_points[trap].x, list_of_points[trap].y])
    begin_vertex.append(vertices[0])
    vertices.extend(begin_vertex)
    return vertices[::-1]

def finding_initial_Point(list_of_points) :
    min = 0
    for i in range(1, len(list_of_points)): 
        if list_of_points[i].y < list_of_points[min].y: 
            min = i 
        elif list_of_points[i].y == list_of_points[min].y: 
            if list_of_points[i].x < list_of_points[min].x : 
                min = i 
    return min

def orientation(fp, sp, tp) :
    value = ((sp.y - fp.y) * (tp.x - sp.x)) - ((sp.x - fp.x) * (tp.y - sp.y))
    if value == 0:   
        return 0  # collinear
    elif value > 0: 
        return 1  # Clockwise
    elif value < 0:
        return 2  # Counterclockwise
  

def output(list_of_points) ->str:
    list1 =[]
    list2 = []
    for i in (tuple(x) for x in list_of_points):
        list1.append(i)
    new_tuple = tuple(list1)
    for i in new_tuple:
        s = str(i)
        list2.append(s)
        polygon_edges = "-".join(list2)
    print(polygon_edges)


import matplotlib.pyplot as plt

def problem_plot(x, y, point_list) :
    plt.scatter(x, y, color = 'black') 
    plt.xlabel("X - axis")
    plt.ylabel("Y - axis")#for plotting the points.
    x_points = []
    y_points = []   
    points = [z for y in point_list for z in y]
    x_points = points[::2]
    y_points = points[1::2]
    plt.plot(x_points, y_points, color='red')
    plt.legend(["Edge of Polygon", "Moth Trap"], loc ="upper right")
    plt.show()

for i in (0,1000):
    no_of_traps = int(input())
    if no_of_traps < 3:
      break
    vertices_of_polygon, x_points, y_points = inputs(no_of_traps)
    print(_)
    print("Region #", area, ":")
    output(vertices_of_polygon)
    print("Perimeter length = ", finding_perimeter(vertices_of_polygon))
    traps_plot(x_points, y_points, vertices_of_polygon) 
   
